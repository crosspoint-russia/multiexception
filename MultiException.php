<?php
/**
 * Created by PhpStorm.
 * User: Тимур
 * Date: 30.01.2017
 * Time: 10:04
 */

namespace CrosspointRussia\Multiexception;


class MultiException extends \Exception
    implements \Iterator
{
    use IteratorTrait;

    public function add(\Exception $exception)
    {
        $this->data[] = $exception;
    }

    public function isEmpty():bool
    {
        return empty($this->data);
    }
}